import keras
from keras_self_attention import SeqSelfAttention
import pandas as pd


def attention_model():
    model = keras.models.Sequential()
    model.add(keras.layers.LSTM(cfg.LSTM, input_shape=(cfg.TIMESTEPS,
              cfg.FEATURES),
              return_sequences=True))
    model.add(SeqSelfAttention(attention_activation='sigmoid'))
    model.add(keras.layers.Dense(cfg.DENSE))
    model.add(keras.layers.Dense(OUTPUT, activation='sigmoid'))
    return model
    
    
def save_result(model, X_test, label):
    y_test_proba_lstm = model.predict_proba(X_test)[:, 1]
    submission = pd.DataFrame({'y_pred': y_test_proba_lstm})
    submission.index.name = 'id'
    submission.to_csv('submission_%s.csv'%label)
    print("saved the result to 'submission_%s.csv"%label)
    
def combine_results(path):
    import pandas as pd
    import glob
    all_files = glob.glob(path + "/*.csv")
    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    all_results = pd.concat(li, axis=0, ignore_index=True)
    return all_results
        